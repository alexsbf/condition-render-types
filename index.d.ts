type ConditionProps = {
    activeId: string | number,
    children: JSX.Element[] | JSX.Element
}

declare const Conditional: React.FC<ConditionProps>

export default Conditional